import * as types from '../../actions/action-types';


const testComponentState = {
  testComponentData: {
    testProperties: '',
    chartData: {},
    tableData: {}
  }
}


const testComponentReducer = function(state = initialState, action) {
  switch(action.type) {

    case types.SET_TEST_COMPONENT_PROPERTY:
      return Object.assign({}, state, {
        testComponentData: Object.assign({}, state.testComponentData, {
          testProperties: action.data
        })
      });

    /*
      Сюда добавляем установку chartData и tableData
    */

    default: return state;
  }
}


export default testComponentReducer;
